#ifndef MENU_H_
#define MENU_H_
/*
    example of use:
    int opt = getMenuOption("opt 1", "opt 2", "opt 3", NULL);
	printf("opt chose ----> %d\n", opt);
*/
int getMenuOption(const char* firstStr, ...);

#endif