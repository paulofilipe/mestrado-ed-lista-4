#include <stdio.h>
#include <stdlib.h>

#include "lista.h"

typedef struct hash Hash;

struct hash {
    int keysSize;
    int bucketSize;
    List **hashArray;
};

int hashCode(int value, Hash* hash) {
    if (value <= hash->bucketSize) {
        return 1;
    }
   return value/hash->bucketSize + value % hash->bucketSize;
}

Hash *createHash(int qtdKeys, int bucketSize) {
	int i;

	if( qtdKeys < 1 ) {
        printf("\nVALOR INVÀLIDO\n");
        return NULL;
    } 
    
    Hash *hash = malloc( sizeof(hash));
	hash->keysSize = qtdKeys;
    hash->bucketSize = bucketSize;

    hash->hashArray = malloc( sizeof(List *) * hash->keysSize);

	for( i = 1; i <= hash->keysSize; i++ ) {
		hash->hashArray[i] = NULL;
	}

	return hash;
}

int getSize(Hash *hash) {
    return hash->keysSize * hash->bucketSize;
}

void insert(Hash *hash, int val) {
    int key = hashCode(val, hash);
    if (hash->hashArray[key] != NULL && findInList(hash->hashArray[key], val) != NULL) {
        printf("valor já inserido");
        return;
    }
    hash->hashArray[key] = insertInList(hash->hashArray[key], val);
}

void find(int val, Hash *hash) {
    int key = hashCode(val, hash);

    if (hash->hashArray[key] == NULL) {
        printf("\nValor %d não encontrado\n", val);
        return;
    }

    List *bucket = findInList(hash->hashArray[key], val);
    if (bucket == NULL) {
        printf("\nValor %d não encontrado\n", val);
        return;
    }

    printf("\nvalor %d encontrado\n", val);
}

void removeOfHash(int val, Hash *hash) {
    int key = hashCode(val, hash);

    if (hash->hashArray[key] == NULL) {
        printf("\nValor %d não encontrado\n", val);
        return;
    }

    hash->hashArray[key] = removeItem(hash->hashArray[key], val);
}

Hash *freeHash(Hash *hash) {
    for (int i = 1; i <= hash->keysSize; i++){
        hash->hashArray[i] = freeList(hash->hashArray[i]);
    }

    free(hash);
    return NULL;
}

void printHash(Hash *hash){
    for(int i = 1;i <= hash->keysSize; i++)
    {
        if (hash->hashArray[i] != NULL) {
            printf("\n'Hash key': '%d'\n'hash bucket':[\n", i);
            printListInInsertionOrder(hash->hashArray[i]);
            printf("\n]\n");
        }
    }
}