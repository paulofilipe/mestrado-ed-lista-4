#ifndef HASH_H
#define HASH_H

typedef struct hash Hash;

Hash *createHash(int ktdKeys, int bucketSize);

int getSize(Hash *hash);

void insert(Hash *hash, int val);

void find(int val, Hash *hash);

void removeOfHash(int val, Hash *hash);

Hash *freeHash(Hash *hash);

void printHash(Hash *hash);

#endif  //HASH_H