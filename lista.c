#include <stdio.h>
#include <stdlib.h>

#include "lista.h"
typedef struct list List;

struct list
{
    int info;
    List *next;
};

void printOneitem(List *item)
{
    printf("{\n\t'hasNext':'%s';\n\t'itemContent':'%d'\n%s",
           item->next != NULL ? "true" : "false",
           item->info,
           item->next != NULL ? "},\n" : "}");
}

int isEmpty(List *list)
{
    return (list == NULL);
}

List *createList()
{
    return NULL;
}

List *insertInList(List *list, int val)
{
    List *newItem = (List *)malloc(sizeof(List));
    newItem->info = val;
    newItem->next = list;
    return newItem;
}

void printListInInsertionOrder(List *list)
{
    if (isEmpty(list))
    {
        printf("\n\nEmpty list\n\n");
        return;
    }

    List *temp;
    for (temp = list; temp != NULL; temp = temp->next)
    {
        printOneitem(temp);
    }
}

List *findInList(List *list, int val)
{
    List *temp;
    for (temp = list; temp != NULL; temp = temp->next)
    {
        if (temp->info == val)
            return temp;
    }

    return NULL;
}

List *removeItem(List *list, int val)
{
    List *prev = NULL;
    List *temp = list;

    while (temp != NULL && temp->info != val)
    {
        prev = temp;
        temp = temp->next;
    }

    if (temp == NULL)
    {
        printf("item não encontrado");
        return list;
    }

    printf("Valor %d encontrado", val);

    if (prev == NULL)
    {
        list = temp->next;
    }
    else
    {
        prev->next = temp->next;
    }

    free(temp);
    return list;
}

List *freeList(List *list)
{
    List *temp = list;
    while (temp != NULL)
    {
        List *next = temp->next;
        free(temp);
        temp = next;
    }

    return NULL;
}