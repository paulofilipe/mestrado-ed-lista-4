#include <stdio.h>
#include <stdlib.h>

#include "menu.h"
#include "hash.h"

int readFromKeyBoard()
{
	int value;
	int ch;
	scanf(" %d", &value);
	while ((ch = getchar()) != '\n')
		;
	return value;
}

int main(void)
{
	
	int loopFlag = 1;
	int value;

	Hash *hash = NULL;

	while (loopFlag)
	{

		int opt = getMenuOption("Criar a estrutura de dados (hash estendivel)",
								"Inserir elemento",
								"Recuperar/Buscar um determinado elemento",
								"Remover um determinado elemento",
								"Liberar a estrutura de dados (hash estendivel)",
								"Imprimir hash",
								NULL);
		printf("\nopt chose ----> %d\n", opt);

		switch (opt)
		{
		case 1:
			printf("\nDigite tamanho a quantidade de chaves\n");
			value = readFromKeyBoard();
			printf("\nDigite tamanho do bucket\n");
			int bucketSize = readFromKeyBoard();
			hash = createHash(value, bucketSize);
			break;
		case 2:
			if (hash == NULL) {
				printf("\nHASH NÃO INICIALIZADO\n");
				continue;
			}
			printf("\nDigite valor a ser inserido: \n");
			value = readFromKeyBoard();
			if (value > getSize(hash) || value < 1) {
				printf("\n valor fora do hange de valores possiveis\n");
			} else {
				insert(hash, value);
			}
			break;
		case 3:
			if (hash == NULL) {
				printf("\nHASH NÃO INICIALIZADO\n");
				continue;
			}
			printf("\nDigite o valor da busca: \n");
			value = readFromKeyBoard();
			find(value, hash);
			break;
		case 4:
			if (hash == NULL) {
				printf("\nHASH NÃO INICIALIZADO\n");
				continue;
			}

			printf("\nDigite o valor a ser removido: \n");
			value = readFromKeyBoard();
			removeOfHash(value, hash);
			break;
		case 5:
			if (hash == NULL) {
				printf("\nHASH NÃO INICIALIZADO\n");
				continue;
			}
			hash = freeHash(hash);
			break;
		case 6:
			if (hash == NULL) {
				printf("\nHASH NÃO INICIALIZADO\n");
				continue;
			}
			printHash(hash);
			break;
		default:
			loopFlag = 0;
			break;
		}
	}
	getchar();
	return 0;
}